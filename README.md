I have implemented POM framework with Maven and Testng based on broswers. This framework is dynamic but still lots of things to do + refactoring and dymainc initializing of the target broswers. 
It also supports execution of tests through Selenium GRID.


-------------------SETUP URL----------------------------------------
Adjust date in following URL in Constants.java class -- DATEs can be automated to pick future date. I can call URL BUILDER for which we can pass parameter dynamic.
public static String EncoreURL = "https://www.encoretickets.co.uk/booking/seating-plan?product_id=6362&content_product_id=6362"
			+ "&product_type=show"
			+ "&performance_type=E"
			+ "&venue_id=112&qt=2"
			+ "&slot=19:30" // adjust time 
			+ "&date=2021-08-26&booking_ends=2022-06-26" // amend these dates for future
			+ "&booking_starts=2021-08-26" //amend this dates for future


-------------------Setup of ChromeDriver path-------------------------
Just download the chrome.exe driver based on your chrome browser version. 
https://chromedriver.storage.googleapis.com/index.html?path=93.0.4577.15/ download for windows from this site
Copy your chromdriver.exe path and paste in KorrsDriver.java class under com.mk.web package mentioned below
System.setProperty("webdriver.chrome.driver", "{chromedriver.exe path}");
Save it and clean and build it. All set to run the tests

----------------Execution of Tests-------------------------------------
There is EncoreTest.java class under src/test/java package
I have just created two tests 1) VerifySelectedSeatsInNormalView and 2) VerifySelectedSeatsInRestrictedView . 
Note : -> Please see the logic and I'll explain you in detail also while demoing. If you like it.

-------------------Reports---------------------------------------------
Reports are getting saved under reports folder.


NOTE -> Please ignore annotations as of now for the packages name. I have just picked up after long time. 
